//
// Copyright (C) 2004 Andras Varga
// Copyright (C) 2014 OpenSim Ltd.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//

#include <stdlib.h>
#include <string.h>

#include "inet/applications/common/SocketTag_m.h"
#include "inet/common/INETUtils.h"
#include "inet/common/IProtocolRegistrationListener.h"
#include "inet/common/LayeredProtocolBase.h"
#include "inet/common/ModuleAccess.h"
#include "inet/common/ProtocolTag_m.h"
#include "inet/common/lifecycle/NodeOperations.h"
#include "inet/common/lifecycle/NodeStatus.h"
#include "inet/common/serializer/TcpIpChecksum.h"
#include "inet/networklayer/arp/ipv4/ArpPacket_m.h"
#include "inet/networklayer/common/DscpTag_m.h"
#include "inet/networklayer/common/EcnTag_m.h"
#include "inet/networklayer/common/FragmentationTag_m.h"
#include "inet/networklayer/common/HopLimitTag_m.h"
#include "inet/networklayer/common/L3AddressTag_m.h"
#include "inet/networklayer/common/L3Tools.h"
#include "inet/networklayer/common/MulticastTag_m.h"
#include "inet/networklayer/common/NextHopAddressTag_m.h"
#include "inet/networklayer/contract/IArp.h"
#include "inet/networklayer/contract/IInterfaceTable.h"
#include "inet/networklayer/contract/L3SocketCommand_m.h"
#include "inet/networklayer/ipv4/IcmpHeader_m.h"
#include "inet/networklayer/ipv4/IIpv4RoutingTable.h"
#include "inet/networklayer/ipv4/Ipv4Header.h"
#include "inet/networklayer/ipv4/Ipv4InterfaceData.h"
#include "inet/linklayer/common/InterfaceTag_m.h"
#include "inet/linklayer/common/MacAddressTag_m.h"
#include "Ipv4Ext.h"

#include "inet/routing/gpsr/Gpsr.h"
#include "inet/routing/aodv/AodvRouting.h"
#include "../../cache/GeneralCache.h"

namespace inet {

Define_Module(Ipv4Ext);

Ipv4Address Ipv4Ext::getNextHopAddr(Ipv4Address dAddr) {
    Ipv4Address nextAdd = Ipv4Address::UNSPECIFIED_ADDRESS;
    //IRoutingTable *routingTable = dynamic_cast<IRoutingTable *>(this->getModuleByPath("^.ipv4.routingTable"));
    IRoutingTable *routingTable = dynamic_cast<IRoutingTable *>(this->getParentModule()->getSubmodule("routingTable"));

    if (routingTable != nullptr) {
        /*EV << "routingTable FOUND!" << endl;
        IRoute *route = routingTable->findBestMatchingRoute(dAddr);
        EV << "Test: " << routingTable->getNextHopForDestination(dAddr) << endl;
        if (route) {
            EV << "route FOUND!" << endl;
            EV << "ROUTE. NextHop: " << route->getNextHopAsGeneric() << ". Metric:" << route->getMetric() << endl;

            nextAdd = route->getNextHopAsGeneric().toIPv4();
        }*/
        AodvRouting *aodv = dynamic_cast<AodvRouting *>(this->getParentModule()->getParentModule()->getSubmodule("aodv"));
        if (aodv != nullptr) {
            IRoute *route = routingTable->findBestMatchingRoute(dAddr);
            EV << "Test: " << routingTable->getNextHopForDestination(dAddr) << endl;
            if (route) {
                EV << "route FOUND!" << endl;
                EV << "ROUTE. NextHop: " << route->getNextHopAsGeneric() << ". Metric:" << route->getMetric() << endl;
                nextAdd = route->getNextHopAsGeneric().toIPv4();
            }
        }
        else {
            //Gpsr
            Gpsr *gpsr = dynamic_cast<Gpsr *>(this->getParentModule()->getParentModule()->getSubmodule("gpsr"));
            if (gpsr != nullptr) {
                int myAppAddr = this->getParentModule()->getParentModule()->getIndex();
                inet::GeneralCache *gc = check_and_cast<inet::GeneralCache *>(this->getParentModule()->getParentModule()->getParentModule()->getSubmodule("cache"));
                std::list<GeneralCache::RecordElement> *pi_l = gc->getList(myAppAddr, GeneralCache::GPSR);
                if (pi_l != nullptr) {
                    for (auto& gi_s : *pi_l) {
                        simtime_t oldness = simTime() - gi_s.time;
                        if (oldness.dbl() <= 5.0) {
                            Gpsr_Info *gi = static_cast<Gpsr_Info *>(gi_s.info);
                            if ((gi->destAddr == dAddr) && (gi->nextHopAddr.toIPv4() != Ipv4Address::UNSPECIFIED_ADDRESS)) {
                                EV << "ROUTE GPSR. NextHop: " << gi->nextHopAddr << endl;
                                nextAdd = gi->nextHopAddr.toIPv4();
                                break;
                            }
                        }
                    }
                }
            }
        }
    }
    return nextAdd;
}

void Ipv4Ext::sendPacketToNIC(Packet *packet)
{
    EV_INFO << "Sending " << packet << " to output interface = " << ift->getInterfaceById(packet->getMandatoryTag<InterfaceReq>()->getInterfaceId())->getInterfaceName() << ".\n";
    packet->ensureTag<PacketProtocolTag>()->setProtocol(&Protocol::ipv4);
    packet->ensureTag<DispatchProtocolInd>()->setProtocol(&Protocol::ipv4);
    delete packet->removeTag<DispatchProtocolReq>();
    ASSERT(packet->getTag<InterfaceReq>() != nullptr);

    EV << "sendPacketToNIC" << endl;

    int rIdx = this->getParentModule()->getParentModule()->getIndex();
    IP_Info *ii = new IP_Info();
    ii->byteTransmitted = packet->getByteLength();
    ii->pktID = packet->getId();

    Ipv4Address nhAddr = Ipv4Address::UNSPECIFIED_ADDRESS;
    const auto& ipv4Header = packet->peekHeader<Ipv4Header>();
    if (ipv4Header) {
        ii->destAddr = ipv4Header->getDestAddress();
        nhAddr = getNextHopAddr(ii->destAddr);
        EV << "YES ipv4Header : " << ii->destAddr << " packet name: " << packet->getName() << endl;
    }
    else {
        ii->destAddr = Ipv4Address::UNSPECIFIED_ADDRESS;
    }
    ii->nextHopAddr = nhAddr;


    inet::GeneralCache *gc = check_and_cast<inet::GeneralCache *>(this->getParentModule()->getParentModule()->getParentModule()->getSubmodule("cache"));
    gc->addEntry(rIdx, inet::GeneralCache::IP, simTime(), ii);

    EV << "Ipv4Ext -> ID: " << packet->getId() << endl;
    //EV << "Ipv4Ext -> getDataLength: " << packet->getDataLength() << endl;
    EV << "Ipv4Ext -> getByteLength: " << packet->getByteLength() << endl;
    EV << "Ipv4Ext -> dest address: " << ii->destAddr << endl;
    EV << "Ipv4Ext -> nexthop address: " << ii->nextHopAddr << endl;
    //EV << "Ipv4Ext -> getEncapsulatedPacket()->getByteLength(): " << packet->getEncapsulatedPacket()->getByteLength() << endl;
    //EV << "Ipv4Ext -> getHeaderPopOffset: " << packet->getHeaderPopOffset() << endl;
    //EV << "Ipv4Ext -> getHeaderPoppedLength: " << packet->getHeaderPoppedLength() << endl;
    //EV << "Ipv4Ext -> getTrailerPopOffset: " << packet->getTrailerPopOffset() << endl;
    //EV << "Ipv4Ext -> getTrailerPoppedLength: " << packet->getTrailerPoppedLength() << endl;
    //EV << "Ipv4Ext -> getTotalLength: " << packet->getTotalLength() << endl;

    send(packet, "queueOut");
}


void Ipv4Ext::handleReceivedPktForMe(Packet *packet) {

    int rIdx = this->getParentModule()->getParentModule()->getIndex();
    IP_RCV_Info *ii = new IP_RCV_Info();
    ii->byteReceived = packet->getByteLength();
    ii->pktID = packet->getId();

    const auto& ipv4Header = packet->peekHeader<Ipv4Header>();
    if (ipv4Header) {
        ii->srcAddr = ipv4Header->getSrcAddress();
        EV << "YES ipv4Header : " << ii->srcAddr << " packet name: " << packet->getName() << endl;
    }
    else {
        ii->srcAddr = Ipv4Address::UNSPECIFIED_ADDRESS;
    }

    inet::GeneralCache *gc = check_and_cast<inet::GeneralCache *>(this->getParentModule()->getParentModule()->getParentModule()->getSubmodule("cache"));
    gc->addEntry(rIdx, inet::GeneralCache::IP_RCV, simTime(), ii);
}

void Ipv4Ext::reassembleAndDeliverFinish(Packet *packet) {

    handleReceivedPktForMe(packet);

    Ipv4::reassembleAndDeliverFinish(packet);
    /*
    auto ipv4HeaderPosition = packet->getHeaderPopOffset();
    const auto& ipv4Header = packet->peekHeader<Ipv4Header>();
    int protocol = ipv4Header->getProtocolId();
    decapsulate(packet);
    auto lowerBound = protocolIdToSocketDescriptors.lower_bound(protocol);
    auto upperBound = protocolIdToSocketDescriptors.upper_bound(protocol);
    bool hasSocket = lowerBound != upperBound;
    for (auto it = lowerBound; it != upperBound; it++) {
        cPacket *packetCopy = packet->dup();
        packetCopy->ensureTag<SocketInd>()->setSocketId(it->second->socketId);
        emit(packetSentToUpperSignal, packetCopy);
        send(packetCopy, "transportOut");
    }
    if (mapping.findOutputGateForProtocol(protocol) >= 0) {
        //handleReceivedPktForMe(packet);

        emit(packetSentToUpperSignal, packet);
        send(packet, "transportOut");
        numLocalDeliver++;
    }
    else if (hasSocket) {
        //handleReceivedPktForMe(packet);
        delete packet;
    }
    else {
        EV_ERROR << "Transport protocol ID=" << protocol << " not connected, discarding packet\n";
        packet->setHeaderPopOffset(ipv4HeaderPosition);
        const InterfaceEntry* fromIE = getSourceInterface(packet);
        sendIcmpError(packet, fromIE ? fromIE->getInterfaceId() : -1, ICMP_DESTINATION_UNREACHABLE, ICMP_DU_PROTOCOL_UNREACHABLE);
    }*/
}

} // namespace inet

