//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "GeneralCache.h"

namespace inet {

Define_Module(GeneralCache);

GeneralCache::GeneralCache() {
    // TODO Auto-generated constructor stub


}

GeneralCache::~GeneralCache() {
    // TODO Auto-generated destructor stub
}

void GeneralCache::createNewCache(int node, type_cache tc) {
    if (cache.count(node) == 0) {
        std::map<type_cache, std::list<RecordElement> > *nmap = new std::map<type_cache, std::list<RecordElement> >();
        cache[node] = *nmap;
    }

    if (cache[node].count(tc) == 0) {
        std::list<RecordElement> *nlist = new std::list<RecordElement>();
        cache[node][tc] = *nlist;
    }
}

void GeneralCache::addEntry(int node, type_cache tc, simtime_t time, void *data) {
    createNewCache(node, tc);

    if ((tc == PHYISICAL) || (tc == GPSR) || (tc == MAC) || (tc == IP) || (tc == IP_RCV)) {
        RecordElement newR;
        newR.time = time;
        newR.info = data;
        cache[node][tc].push_front(newR);
    }
}

Physical_Info *GeneralCache::getPhysicalEntry(int node, long pktID) {
    if ((cache.count(node) != 0) && (cache[node].count(PHYISICAL) != 0)) {
        for (auto& pp : cache[node][PHYISICAL]) {
            Physical_Info *ppinfo = static_cast<Physical_Info *>(pp.info);
            if (ppinfo->pktID == pktID) {
                return ppinfo;
            }
        }
    }
    return nullptr;
}

std::list<GeneralCache::RecordElement> *GeneralCache::getList(int node, type_cache tc) {

    if ((cache.count(node) != 0) && (cache[node].count(tc) != 0)) {
        std::list<RecordElement> *ris = &(cache[node][tc]);
        return ris;
    }
    return nullptr;
}

void GeneralCache::removeOldCache(simtime_t time, double maxSec) {

    for (auto& n : cache) {
        for (auto& t : n.second) {
            std::list<RecordElement>::iterator it = t.second.begin();
            while (it != t.second.end()) {
                RecordElement *re = &(*it);
                if ((time - re->time).dbl() > maxSec) {
                    it = t.second.erase(it);
                }
                else {
                    ++it;
                }
            }

            /*
            bool removed;
            do {
                removed = false;

                for (auto it = t.second.begin(); it != t.second.end(); it++){
                    RecordElement *re = &(*it);
                    if ((time - re->time).dbl() > maxSec) {
                        t.second.erase(it);
                        removed = true;
                        break;
                    }
                }

            } while(removed);
            */
        }
    }
}


} /* namespace inet */
