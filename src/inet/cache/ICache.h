//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef INET_CACHE_ICACHE_H_
#define INET_CACHE_ICACHE_H_

#include "inet/common/INETDefs.h"
#include <map>
#include <list>

namespace inet {

class INET_API ICache : public cSimpleModule {
public:
    typedef enum {
        GENERIC,
        PHYISICAL,
        MAC,
        IP,
        IP_RCV,
        GPSR
    } type_cache;

    typedef struct {
        simtime_t time;
        void *info;
    } RecordElement;

public:
    ICache(){};
    virtual ~ICache(){};

    virtual void createNewCache(int node, type_cache tc){};
    virtual void addEntry(int node, type_cache tc, simtime_t time, void *data){};


protected:
  /** @name Module */
  //@{
  virtual int numInitStages() const override { return NUM_INIT_STAGES; }
  virtual void initialize(int stage) override;
  //virtual void finish() override;
  //virtual void handleMessage(cMessage *message) override;
  //@}

protected:
    std::map<int, std::map<type_cache, std::list<RecordElement> > > cache;
};

} /* namespace inet */

#endif /* INET_CACHE_ICACHE_H_ */
