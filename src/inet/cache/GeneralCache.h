//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef INET_CACHE_GENERALCACHE_H_
#define INET_CACHE_GENERALCACHE_H_

#include <inet/common/Units.h>
#include <inet/networklayer/common/L3Address.h>
#include "inet/common/INETUtils.h"

#include "ICache.h"

using inet::units::values::W;

namespace inet {

class Physical_Info {
public:
    bool isData;
    int txAddr;
    long pktID;
    W pow;
    double snir;
    double per;
};

class Mac_Info {
public:
    int retransmissions;
    double retransmissionsOverMax;

    unsigned int byteTransmitted;
    long pktID;
    bool isTransmitted;
};

class IP_Info {
public:
    unsigned int byteTransmitted;
    Ipv4Address destAddr;
    Ipv4Address nextHopAddr;
    long pktID;
};

class IP_RCV_Info {
public:
    unsigned int byteReceived;
    Ipv4Address srcAddr;
    long pktID;
};

class Gpsr_Info {
public:
    L3Address destAddr;
    L3Address nextHopAddr;
};

class GeneralCache : public ICache {

public:
    GeneralCache();
    virtual ~GeneralCache();

    virtual void createNewCache(int node, type_cache tc);
    virtual void addEntry(int node, type_cache tc, simtime_t time, void *data);

    virtual std::list<GeneralCache::RecordElement> *getList(int node, type_cache tc);

    virtual Physical_Info *getPhysicalEntry(int node, long pktID);

    virtual void removeOldCache(simtime_t time, double maxSec);
};

} /* namespace inet */

#endif /* INET_CACHE_GENERALCACHE_H_ */
