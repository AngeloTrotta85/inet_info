//
// Copyright (C) 2016 OpenSim Ltd.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//

#include "DcfExt.h"
#include "inet/common/ModuleAccess.h"
#include "inet/linklayer/ieee8022/Ieee8022LlcHeader_m.h"
#include "inet/linklayer/ieee80211/mac/framesequence/DcfFs.h"
#include "inet/linklayer/ieee80211/mac/Ieee80211Mac.h"
#include "inet/linklayer/ieee80211/mac/rateselection/RateSelection.h"
#include "inet/linklayer/ieee80211/mac/recipient/RecipientAckProcedure.h"

#include "../../../../cache/GeneralCache.h"

namespace inet {
namespace ieee80211 {

using namespace inet::physicallayer;

Define_Module(DcfExt);


void DcfExt::originatorProcessTransmittedFrame(Packet *packet)
{
    mac->emit(packetSentToPeerSignal, packet);
    auto transmittedHeader = packet->peekHeader<Ieee80211MacHeader>();
    if (auto dataOrMgmtHeader = dynamicPtrCast<const Ieee80211DataOrMgmtHeader>(transmittedHeader)) {
        EV_INFO << "For the current frame exchange, we have CW = " << dcfChannelAccess->getCw() << " SRC = " << recoveryProcedure->getShortRetryCount(packet, dataOrMgmtHeader) << " LRC = " << recoveryProcedure->getLongRetryCount(packet, dataOrMgmtHeader) << " SSRC = " << stationRetryCounters->getStationShortRetryCount() << " and SLRC = " << stationRetryCounters->getStationLongRetryCount() << std::endl;
        if (originatorAckPolicy->isAckNeeded(dataOrMgmtHeader)) {
            ackHandler->processTransmittedDataOrMgmtFrame(dataOrMgmtHeader);
        }
        else if (dataOrMgmtHeader->getReceiverAddress().isMulticast()) {
            recoveryProcedure->multicastFrameTransmitted(stationRetryCounters);
            inProgressFrames->dropFrame(packet);
        }

        auto transmittedTrailer = packet->peekTrailer<Ieee80211MacTrailer>();
        int rIdx = this->getParentModule()->getParentModule()->getParentModule()->getIndex();
        Mac_Info *mi = new Mac_Info();
        mi->retransmissions = recoveryProcedure->getRetryCount(packet, dataOrMgmtHeader);
        mi->retransmissionsOverMax = ((double)mi->retransmissions) / ((double) recoveryProcedure->getRetryLimit(packet));

        unsigned int bSent = packet->getByteLength() -
                (transmittedHeader->getChunkLength().get() / 8) -
                (transmittedTrailer->getChunkLength().get() / 8) -
                8;
        if (bSent <= 0) {
            bSent = 8;
        }
        mi->byteTransmitted = bSent;
        mi->pktID = packet->getId();
        mi->isTransmitted = true;

        inet::GeneralCache *gc = check_and_cast<inet::GeneralCache *>(this->getParentModule()->getParentModule()->getParentModule()->getParentModule()->getSubmodule("cache"));
        gc->addEntry(rIdx, inet::GeneralCache::MAC, simTime(), mi);

        /*
        EV << "DcfExt -> ID: " << packet->getId() << endl;
        EV << "DcfExt -> bSent: " << bSent << endl;
        EV << "DcfExt -> getDataLength: " << packet->getDataLength() << endl;
        EV << "DcfExt -> getByteLength: " << packet->getByteLength() << endl;
        EV << "DcfExt -> getHeaderPopOffset: " << packet->getHeaderPopOffset() << endl;
        EV << "DcfExt -> getHeaderPoppedLength: " << packet->getHeaderPoppedLength() << endl;
        EV << "DcfExt -> getTrailerPopOffset: " << packet->getTrailerPopOffset() << endl;
        EV << "DcfExt -> getTrailerPoppedLength: " << packet->getTrailerPoppedLength() << endl;
        EV << "DcfExt -> getTotalLength: " << packet->getTotalLength() << endl;
        EV << "DcfExt -> transmittedHeader->getChunkLength(): " << transmittedHeader->getChunkLength() << endl;
        EV << "DcfExt -> transmittedTrailer->getChunkLength(): " << transmittedTrailer->getChunkLength() << endl;
        EV << "DcfExt -> transmittedHeader->getChunkLength().get: " << transmittedHeader->getChunkLength().get() << endl;
        EV << "DcfExt -> transmittedTrailer->getChunkLength().get: " << transmittedTrailer->getChunkLength().get() << endl;

        EV_INFO << "Setting MAC stats. node: " << rIdx << "; Retry: " << mi->retransmissions << "; bSent: " << mi->byteTransmitted << endl;
        */
    }
    else if (auto rtsFrame = dynamicPtrCast<const Ieee80211RtsFrame>(transmittedHeader)) {
        auto protectedFrame = inProgressFrames->getFrameToTransmit(); // TODO: kludge
        auto protectedHeader = protectedFrame->peekHeader<Ieee80211DataOrMgmtHeader>();
        EV_INFO << "For the current frame exchange, we have CW = " << dcfChannelAccess->getCw() << " SRC = " << recoveryProcedure->getShortRetryCount(protectedFrame, protectedHeader) << " LRC = " << recoveryProcedure->getLongRetryCount(protectedFrame, protectedHeader) << " SSRC = " << stationRetryCounters->getStationShortRetryCount() << " and SLRC = " << stationRetryCounters->getStationLongRetryCount() << std::endl;
        rtsProcedure->processTransmittedRts(rtsFrame);
    }
}

void DcfExt::originatorProcessFailedFrame(Packet *failedPacket)
{
    EV_INFO << "Data/Mgmt frame transmission failed\n";
    const auto& failedHeader = failedPacket->peekHeader<Ieee80211DataOrMgmtHeader>();
    ASSERT(failedHeader->getType() != ST_DATA_WITH_QOS);
    ASSERT(ackHandler->getAckStatus(failedHeader) == AckHandler::Status::WAITING_FOR_ACK);
    recoveryProcedure->dataOrMgmtFrameTransmissionFailed(failedPacket, failedHeader, stationRetryCounters);
    bool retryLimitReached = recoveryProcedure->isRetryLimitReached(failedPacket, failedHeader);
    if (dataAndMgmtRateControl) {
        int retryCount = recoveryProcedure->getRetryCount(failedPacket, failedHeader);
        dataAndMgmtRateControl->frameTransmitted(failedPacket, retryCount, false, retryLimitReached);
    }
    ackHandler->processFailedFrame(failedHeader);
    if (retryLimitReached) {
        recoveryProcedure->retryLimitReached(failedPacket, failedHeader);
        inProgressFrames->dropFrame(failedPacket);
        PacketDropDetails details;
        details.setReason(RETRY_LIMIT_REACHED);
        details.setLimit(-1); // TODO:

        auto transmittedHeader = failedPacket->peekHeader<Ieee80211MacHeader>();
        auto transmittedTrailer = failedPacket->peekTrailer<Ieee80211MacTrailer>();
        int rIdx = this->getParentModule()->getParentModule()->getParentModule()->getIndex();
        Mac_Info *mi = new Mac_Info();
        mi->retransmissions = recoveryProcedure->getRetryCount(failedPacket, failedHeader);
        mi->retransmissionsOverMax = ((double)mi->retransmissions) / ((double) recoveryProcedure->getRetryLimit(failedPacket));

        unsigned int bSent = failedPacket->getByteLength() -
                (transmittedHeader->getChunkLength().get() / 8) -
                (transmittedTrailer->getChunkLength().get() / 8) -
                8;
        if (bSent <= 0) {
            bSent = 8;
        }
        mi->byteTransmitted = bSent;
        mi->pktID = failedPacket->getId();
        mi->isTransmitted = false;

        inet::GeneralCache *gc = check_and_cast<inet::GeneralCache *>(this->getParentModule()->getParentModule()->getParentModule()->getParentModule()->getSubmodule("cache"));
        gc->addEntry(rIdx, inet::GeneralCache::MAC, simTime(), mi);

        emit(packetDropSignal, failedPacket, &details);
        emit(linkBreakSignal, failedPacket);
    }
    else {
        auto h = failedPacket->removeHeader<Ieee80211DataOrMgmtHeader>();
        h->setRetry(true);
        failedPacket->insertHeader(h);
    }
}

} // namespace ieee80211
} // namespace inet

