/*
 * NodeLocalizatorSweep.cpp
 *
 *  Created on: Apr 12, 2018
 *      Author: angelo
 */

#include "NodeLocalizatorSweep.h"

NodeLocalizatorSweep::NodeLocalizatorSweep() {
    // TODO Auto-generated constructor stub

}

NodeLocalizatorSweep::~NodeLocalizatorSweep() {
    // TODO Auto-generated destructor stub
}

void NodeLocalizatorSweep::estimatePos(std::map< int, inet::Coord > &estimatedPositions,
        std::map<int, std::map<int, matrix_info_t> > &matrixMapInfo,
        std::map<int, std::map<int, matrix_info_t> > &simmetricMatrixMapInfo) {
    std::list< std::pair<int, inet::Coord> > ris3Points;

    findAndAddFirst3Node(ris3Points, simmetricMatrixMapInfo);

    if (ris3Points.size() == 3) {
        for (auto& n3 : ris3Points) {
            estimatedPositions[n3.first] = n3.second;
        }
    }
}
