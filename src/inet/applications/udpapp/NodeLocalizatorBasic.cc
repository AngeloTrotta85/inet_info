/*
 * NodeLocalizatorSweep.cpp
 *
 *  Created on: Apr 12, 2018
 *      Author: angelo
 */

#include "NodeLocalizatorBasic.h"

NodeLocalizatorBasic::NodeLocalizatorBasic() {
    // TODO Auto-generated constructor stub

}

NodeLocalizatorBasic::~NodeLocalizatorBasic() {
    // TODO Auto-generated destructor stub
}

void NodeLocalizatorBasic::estimatePos(std::map< int, inet::Coord > &estimatedPositions,
        std::map<int, std::map<int, matrix_info_t> > &matrixMapInfo,
        std::map<int, std::map<int, matrix_info_t> > &simmetricMatrixMapInfo) {

}


#define BEST_ANGLE 60.0
#define BEST_ANGLE_SIGMA 30
#define BEST_ANGLE_MULT 3.0

#define BEST_DISTANCE 5.0
#define BEST_DISTANCE_SIGMA 50.0
#define BEST_DISTANCE_MULT 1.0

void NodeLocalizatorBasic::findAndAddFirst3Node(std::list< std::pair<int, inet::Coord> > &ris3Points, std::map<int, std::map<int, matrix_info_t> > &distMMap) {
    int f1, f2, f3;
    double maxValue = 0;

    ris3Points.clear();

    for (auto& m1 : distMMap) {
        for (auto& m2 : distMMap){
            for (auto& m3 : distMMap){
                if ((m1.first != m2.first) && (m1.first != m3.first) && (m2.first != m3.first)) {
                    double ab, bc, ac;
                    double abc, bca, cab;
                    double actValue = 0;

                    ab = distMMap[m1.first][m2.first].declaredDistance;
                    bc = distMMap[m2.first][m3.first].declaredDistance;
                    ac = distMMap[m1.first][m3.first].declaredDistance;

                    if (    (ab < std::numeric_limits<double>::max()) &&
                            (bc < std::numeric_limits<double>::max()) &&
                            (ac < std::numeric_limits<double>::max()) ) {

                        abc = acos(((ab*ab) + (bc*bc) - (ac*ac)) / (2.0 * ab * bc));
                        bca = acos(((bc*bc) + (ac*ac) - (ab*ab)) / (2.0 * bc * ac));
                        cab = acos(((ac*ac) + (ab*ab) - (bc*bc)) / (2.0 * ac * ab));

                        /*
                        actValue += BEST_ANGLE_MULT * (1.0/(BEST_ANGLE_SIGMA*sqrt(2.0*M_PI))) * exp( -0.5 * ( pow( (abc - BEST_ANGLE) / BEST_ANGLE_SIGMA, 2.0)) );
                        actValue += BEST_ANGLE_MULT * (1.0/(BEST_ANGLE_SIGMA*sqrt(2.0*M_PI))) * exp( -0.5 * ( pow( (bca - BEST_ANGLE) / BEST_ANGLE_SIGMA, 2.0)) );
                        actValue += BEST_ANGLE_MULT * (1.0/(BEST_ANGLE_SIGMA*sqrt(2.0*M_PI))) * exp( -0.5 * ( pow( (cab - BEST_ANGLE) / BEST_ANGLE_SIGMA, 2.0)) );

                        actValue += BEST_DISTANCE_MULT * (1.0/(BEST_DISTANCE_SIGMA*sqrt(2.0*M_PI))) * exp( -0.5 * ( pow( (ab - BEST_DISTANCE) / BEST_DISTANCE_SIGMA, 2.0)) );
                        actValue += BEST_DISTANCE_MULT * (1.0/(BEST_DISTANCE_SIGMA*sqrt(2.0*M_PI))) * exp( -0.5 * ( pow( (bc - BEST_DISTANCE) / BEST_DISTANCE_SIGMA, 2.0)) );
                        actValue += BEST_DISTANCE_MULT * (1.0/(BEST_DISTANCE_SIGMA*sqrt(2.0*M_PI))) * exp( -0.5 * ( pow( (ac - BEST_DISTANCE) / BEST_DISTANCE_SIGMA, 2.0)) );
                         */

                        actValue += BEST_ANGLE_MULT * exp( -0.5 * ( pow( (abc - BEST_ANGLE) / BEST_ANGLE_SIGMA, 2.0)) );
                        actValue += BEST_ANGLE_MULT * exp( -0.5 * ( pow( (bca - BEST_ANGLE) / BEST_ANGLE_SIGMA, 2.0)) );
                        actValue += BEST_ANGLE_MULT * exp( -0.5 * ( pow( (cab - BEST_ANGLE) / BEST_ANGLE_SIGMA, 2.0)) );

                        actValue += BEST_DISTANCE_MULT * exp( -0.5 * ( pow( (ab - BEST_DISTANCE) / BEST_DISTANCE_SIGMA, 2.0)) );
                        actValue += BEST_DISTANCE_MULT * exp( -0.5 * ( pow( (bc - BEST_DISTANCE) / BEST_DISTANCE_SIGMA, 2.0)) );
                        actValue += BEST_DISTANCE_MULT * exp( -0.5 * ( pow( (ac - BEST_DISTANCE) / BEST_DISTANCE_SIGMA, 2.0)) );

                        if (actValue > maxValue) {
                            maxValue = actValue;
                            f1 = m1.first;
                            f2 = m2.first;
                            f3 = m3.first;
                        }
                    }
                }
            }
        }
    }

    if (maxValue > 0) {
        double ab, bc, ac, alpha;

        ab = distMMap[f1][f2].declaredDistance;
        bc = distMMap[f2][f3].declaredDistance;
        ac = distMMap[f1][f3].declaredDistance;
        alpha = ((ab*ab) + (ac*ac) - (bc*bc))/(2.0 * ab * ac);

        ris3Points.push_back(std::make_pair(f1, inet::Coord::ZERO));
        ris3Points.push_back(std::make_pair(f2, inet::Coord(ab, 0.0)));
        ris3Points.push_back(std::make_pair(f3, inet::Coord(ac * alpha, ac * sqrt(1 - pow(alpha, 2.0)))));
    }
}


void NodeLocalizatorBasic::circleIntersection (inet::Coord &pu1, inet::Coord &pu2, inet::Coord &pv, double duv, inet::Coord &pw, double duw) {
    double r, R, d, dx, dy, cx, cy, Cx, Cy;

    pu1 = pu2 = inet::Coord::NIL;

    if (duv < duw) {
        r  = duv;  R = duw;
        cx = pv.x; cy = pv.y;
        Cx = pw.x; Cy = pw.y;
    } else {
        r  = duw; R  = duv;
        Cx = pv.x; Cy = pv.y;
        cx = pw.x; cy = pw.y;
    }

    // Compute the vector <dx, dy>
    dx = cx - Cx;
    dy = cy - Cy;

    // Find the distance between two points.
    d = sqrt( dx*dx + dy*dy );

    // There are an infinite number of solutions
    // Seems appropriate to also return null
    if ((d < EPSILON) && (abs(R-r) < EPSILON)) {
        return;
    }
    // No intersection (circles centered at the
    // same place with different size)
    else if (d < EPSILON) {
        return;
    }

    double x = (dx / d) * R + Cx;
    double y = (dy / d) * R + Cy;
    inet::Coord P = inet::Coord(x, y);

    // Single intersection (kissing circles)
    if ((abs((R+r)-d) < EPSILON) || (abs(R-(r+d)) < EPSILON)) {
        pu1 = P;
        return;
    }

    // No intersection. Either the small circle contained within
    // big circle or circles are simply disjoint.
    if ( ((d+r) < R) || ((R+r < d)) ) {
        return;
    }

    inet::Coord C = inet::Coord(Cx, Cy);
    double angle = acossafe((r*r-d*d-R*R)/(-2.0*d*R));
    pu1 = rotatePoint(C, P, +angle);
    pu2 = rotatePoint(C, P, -angle);
}



