/*
 * NodeLocalizatorSpring.h
 *
 *  Created on: Apr 12, 2018
 *      Author: angelo
 */

#ifndef INET_APPLICATIONS_UDPAPP_NODELOCALIZATORSPRING_H_
#define INET_APPLICATIONS_UDPAPP_NODELOCALIZATORSPRING_H_

#include "NodeLocalizatorBasic.h"

class NodeLocalizatorSpring : public NodeLocalizatorBasic {
public:
    typedef struct {
        int nodeAddr;
        inet::Coord pos;
        double distance;
    } search_point_t;

public:
    NodeLocalizatorSpring();
    virtual ~NodeLocalizatorSpring();

    virtual void estimatePos(std::map< int, inet::Coord > &estimatedPositions,
                std::map<int, std::map<int, matrix_info_t> > &matrixMapInfo,
                std::map<int, std::map<int, matrix_info_t> > &simmetricMatrixMapInfo);

protected:
    int getMostConnectedPoint(std::list<int> &toDoList,
            std::map< int, inet::Coord > &estimatedPositions,
            std::map<int, std::map<int, matrix_info_t> > &simmetricMatrixMapInfo);

    inet::Coord estimateNextNodePosition(int nodeAddr,
            std::map<int, inet::Coord > &estimatedPositions,
            std::map<int, std::map<int, matrix_info_t> > &simmetricMatrixMapInfo);
};

#endif /* INET_APPLICATIONS_UDPAPP_NODELOCALIZATORSPRING_H_ */
