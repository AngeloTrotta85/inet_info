//
// Copyright (C) 2000 Institut fuer Telematik, Universitaet Karlsruhe
// Copyright (C) 2004,2011 Andras Varga
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//

#ifndef __INET_UDPBASICBEACONINGAPP_H
#define __INET_UDPBASICBEACONINGAPP_H

#include <vector>

#include "inet/common/INETDefs.h"

#include "inet/applications/base/ApplicationBase.h"
#include "inet/transportlayer/contract/udp/UdpSocket.h"
#include "inet/mobility/contract/IMobility.h"

#include "../base/ApplicationBaseBeacon_m.h"

#include "../../cache/GeneralCache.h"

#include "NodeLocalizatorSweep.h"
#include "NodeLocalizatorSpring.h"

namespace inet {

/**
 * UDP application. See NED for more info.
 */
class INET_API UdpBasicBeaconingApp : public ApplicationBase
{
public:

    typedef struct {
        Coord pos;
        std::list<Coord> anchestorsByDepth;
    } sweep_position_t;

    typedef struct {
        int addr;
        std::list<sweep_position_t> positions;
    } sweep_node_t;

    typedef struct {
        double declaredDistance;
    } matrix_spring_t;

    typedef struct {
        int appAddr;
        unsigned int lastSequenceNumber;
        simtime_t timestamp_lastSeen;
        std::vector<node_neigh_info_t> neigh_info;
    } neigh_info_t;

    typedef struct {
        int appAddr;
        Coord estimatedPos;
        double estimatedDist;
    } neigh_extra_info_t;

    inline friend std::ostream& operator<< (std::ostream& stream, const neigh_extra_info_t& nei) {
        stream  << "AppAddr: " << nei.appAddr << "\t"
                << "EstPos: " << nei.estimatedPos << "\t"
                << "EstDist: " << nei.estimatedDist;
        return stream;
    }

public:
    static double watt2dBm(double w) {
        return (10.0 * log10( w ) + 30.0);
    }

    static void polar2cartesian(double t, double l, double &x, double &y) {
        x = cos(t) * l;
        y = sin(t) * l;
    }

    static void cartesian2polar(double x, double y, double &t, double &l) {
        l = hypot(x, y);
        t = atan2(y, x);
    }

    // Due to double rounding precision the value passed into the Math.acos
    // function may be outside its domain of [-1, +1] which would return
    // the value NaN which we do not want.
    static double acossafe(double x) {
      if (x >= +1.0) return 0;
      if (x <= -1.0) return M_PI;
      return acos(x);
    }

    // Rotates a point about a fixed point at some angle 'a'
    static Coord rotatePoint(Coord fp, Coord pt, double a) {
      double x = pt.x - fp.x;
      double y = pt.y - fp.y;
      double xRot = x * cos(a) + y * sin(a);
      double yRot = y * cos(a) - x * sin(a);
      return Coord(fp.x+xRot, fp.y+yRot);
    }

protected:
    enum SelfMsgKinds { START = 1, SEND, STOP };

    // parameters
    std::vector<L3Address> destAddresses;
    std::vector<std::string> destAddressStr;
    int localPort = -1, destPort = -1;
    simtime_t startTime;
    simtime_t stopTime;
    const char *packetName = nullptr;

    simtime_t neghOldTime;
    simtime_t cacheTimeout;

    // state
    UdpSocket socket;
    cMessage *selfMsg = nullptr;
    cMessage *auto1sec = nullptr;

    int myAppAddr;
    Ipv4Address myIPAddr;
    std::vector<Ipv4Address> addressTable;

    GeneralCache *gc;
    IMobility *mob;

    std::map<int, std::list<neigh_info_t>> neighMap;
    std::map<int, neigh_extra_info_t> neighMap_extra;

    // statistics
    int numSent = 0;
    int numReceived = 0;

  protected:
    virtual int numInitStages() const override { return NUM_INIT_STAGES; }
    virtual void initialize(int stage) override;
    virtual void handleMessageWhenUp(cMessage *msg) override;
    virtual void finish() override;
    virtual void refreshDisplay() const override;

    // chooses random destination address
    virtual L3Address chooseDestAddr();
    virtual Packet *createPacket();
    virtual void sendPacket();
    virtual void processPacket(cPacket *msg);
    virtual void setSocketOptions();

    virtual void processStart();
    virtual void processSend();
    virtual void processStop();

    virtual bool handleNodeStart(IDoneCallback *doneCallback) override;
    virtual bool handleNodeShutdown(IDoneCallback *doneCallback) override;
    virtual void handleNodeCrash() override;

    virtual void manageReceivedBeacon(Packet *pk);

    virtual void manage_auto1sec(void);
    virtual void updateNeighMap(void);

    void updatePosEstimation(void);
    void updatePosEstimation_old(void);
    void updateDistEstimation(int nodeAppAddr);

    void estimatePos_phase1(std::map< int, Coord > &estPos, std::map<int, std::map<int, matrix_spring_t> > &distMMap, std::list< std::pair<int, double> > &myNeigList);
    void findAndAddFirst3Node(int &f1, int &f2, int &f3, std::map<int, std::map<int, matrix_spring_t> > &distMMap, std::map< int, Coord > &estPos);
    void executeSweep(std::list<int> &okList, std::list<int> &toAddList, std::map<int, std::map<int, matrix_spring_t> > &simmetricDistMMap, std::map< int, Coord > &estPos);
    void order(std::list<sweep_node_t> &orderL, std::list<sweep_node_t> &okList, std::list<sweep_node_t> &toAddList, std::map<int, std::map<int, matrix_spring_t> > &simmetricDistMMap);
    void bilaterate (sweep_node_t *u, sweep_node_t *v, sweep_node_t *w, std::map<int, std::map<int, matrix_spring_t> > &simmetricDistMMap);
    void update_bilateration (sweep_node_t *u, sweep_node_t *v, std::map<int, std::map<int, matrix_spring_t> > &simmetricDistMMap);
    bool consistent (sweep_position_t *u, sweep_position_t *v);
    void circleIntersection (sweep_position_t &pu1, sweep_position_t &pu2, sweep_position_t &pv, double duv, sweep_position_t &pw, double duw);

    void estimatePos_phase1_1statt(std::map< int, Coord > &estPos, std::map<int, std::map<int, matrix_spring_t> > &distMMap, std::list< std::pair<int, double> > &myNeigList);
    void getnextToAdd(int &risAddr, int &numNeigh, std::list< std::pair<int, double> > &toAddList,
            std::list< int > &addedList, std::map<int, std::map<int, matrix_spring_t> > &simmetricDistMMap);

  private:
    void debugFilePrint(std::map< int, Coord > &mapC);

  public:
    UdpBasicBeaconingApp() {}
    ~UdpBasicBeaconingApp();

    void calculatePhyStats(double &snir, double &pow, double &per, int data1_control2_all3, int nodeAdd, double sec);
};

} // namespace inet

#endif // ifndef __INET_UDPBASICBEACONINGAPP_H

