/*
 * NodeLocalizatorSpring.cc
 *
 *  Created on: Apr 12, 2018
 *      Author: angelo
 */

#include "NodeLocalizatorSpring.h"

NodeLocalizatorSpring::NodeLocalizatorSpring() {
    // TODO Auto-generated constructor stub

}

NodeLocalizatorSpring::~NodeLocalizatorSpring() {
    // TODO Auto-generated destructor stub
}

void NodeLocalizatorSpring::estimatePos(std::map< int, inet::Coord > &estimatedPositions,
        std::map<int, std::map<int, matrix_info_t> > &matrixMapInfo,
        std::map<int, std::map<int, matrix_info_t> > &simmetricMatrixMapInfo) {

    std::list< std::pair<int, inet::Coord> > ris3Points;
    findAndAddFirst3Node(ris3Points, simmetricMatrixMapInfo);
    if (ris3Points.size() == 3) {
        for (auto& n3 : ris3Points) {
            estimatedPositions[n3.first] = n3.second;
        }
    }

    std::list<int> toDoList;
    for (auto& ep : estimatedPositions) {
        if (ep.second.isUnspecified()) {
            toDoList.push_back(ep.first);
        }
    }

    while (toDoList.size() > 0) {
        int nextP = getMostConnectedPoint(toDoList, estimatedPositions, simmetricMatrixMapInfo);
        inet::Coord nextCoord = estimateNextNodePosition(nextP, estimatedPositions, simmetricMatrixMapInfo);

        estimatedPositions[nextP] = nextCoord;
        toDoList.remove(nextP);
    }
}

int NodeLocalizatorSpring::getMostConnectedPoint(std::list<int> &toDoList,
        std::map<int, inet::Coord > &estimatedPositions,
        std::map<int, std::map<int, matrix_info_t> > &simmetricMatrixMapInfo) {

    int maxConnections = -1;
    int ris = -1;

    for (auto& td : toDoList) {
        int actConn = 0;

        for (auto& ep : estimatedPositions) {
            if (!ep.second.isUnspecified()) {
                if ((simmetricMatrixMapInfo.count(td) > 0) && (simmetricMatrixMapInfo[td].count(ep.first))) {
                    if (simmetricMatrixMapInfo[td][ep.first].declaredDistance < std::numeric_limits<double>::max()) {
                        ++actConn;
                    }
                }
            }
        }

        if (actConn > maxConnections) {
            maxConnections = actConn;
            ris = td;
        }
    }

    return ris;
}

bool compare_points (const NodeLocalizatorSpring::search_point_t& firstP, const NodeLocalizatorSpring::search_point_t& secondP) {
  return ( firstP.distance > secondP.distance );
}

inet::Coord NodeLocalizatorSpring::estimateNextNodePosition(int nodeAddr,
        std::map<int, inet::Coord > &estimatedPositions,
        std::map<int, std::map<int, matrix_info_t> > &simmetricMatrixMapInfo) {

    inet::Coord ris = inet::Coord::NIL;

    std::list< search_point_t > neighPoints;

    for (auto& ep : estimatedPositions) {
        if (!ep.second.isUnspecified()) {
            if ((simmetricMatrixMapInfo.count(nodeAddr) > 0) && (simmetricMatrixMapInfo[nodeAddr].count(ep.first))) {
                if (simmetricMatrixMapInfo[nodeAddr][ep.first].declaredDistance < std::numeric_limits<double>::max()) {
                    search_point_t nsp;

                    nsp.nodeAddr = ep.first;
                    nsp.pos = ep.second;
                    nsp.distance = simmetricMatrixMapInfo[nodeAddr][ep.first].declaredDistance;

                    neighPoints.push_back( nsp );
                }
            }
        }
    }
    neighPoints.sort(compare_points);

    std::vector < std::pair <inet::Coord, int> > coordList;

    std::list< search_point_t >::iterator itNP1 = neighPoints.begin();
    while(itNP1 != neighPoints.end()) {
        std::list< search_point_t >::iterator itNP2 = itNP1;
        itNP2++;
        while(itNP2 != neighPoints.end()) {
            inet::Coord pu1, pu2;

            circleIntersection(pu1, pu2, itNP1->pos, itNP1->distance, itNP2->pos, itNP2->distance);
            if ((!pu1.isUnspecified()) && (!pu2.isUnspecified())) {
                int pu1_index, pu2_index;
                double closest = std::numeric_limits<double>::max();

                pu1_index = pu2_index = -1;

                for (unsigned int i = 0; i < coordList.size(); ++i) {
                    double dist1, dist2;

                    dist1 = pu1.distance(coordList[i].first);
                    dist2 = pu2.distance(coordList[i].first);

                    if (dist1 < closest) {
                        pu1_index = i;
                        pu2_index = -1;
                        closest = dist1;
                    }
                    if (dist2 < closest) {
                        pu1_index = -1;
                        pu2_index = i;
                        closest = dist2;
                    }
                }

                if (pu1_index < 0) {
                    coordList.resize(coordList.size() + 1);
                    coordList[coordList.size() - 1] = std::make_pair(pu1, 1);
                }
                else {
                    int newNumber = coordList[pu1_index].second + 1;
                    inet::Coord newAvgCoord = ((coordList[pu1_index].first * ((double)coordList[pu1_index].second)) + pu1) / ((double) newNumber);
                    coordList[pu1_index] = std::make_pair(newAvgCoord, newNumber);
                }

                if (pu2_index < 0) {
                    coordList.resize(coordList.size() + 1);
                    coordList[coordList.size() - 1] = std::make_pair(pu2, 1);
                }
                else {
                    int newNumber = coordList[pu2_index].second + 1;
                    inet::Coord newAvgCoord = ((coordList[pu2_index].first * ((double)coordList[pu2_index].second)) + pu2) / ((double) newNumber);
                    coordList[pu2_index] = std::make_pair(newAvgCoord, newNumber);
                }
            }
            itNP2++;
        }
        itNP1++;
    }

    if (coordList.size() > 0) {
        int maxP = -1;
        for (auto& cl : coordList) {
            if (cl.second > maxP) {
                maxP = cl.second;
                ris = cl.first;
            }
        }
    }

    return ris;
}




