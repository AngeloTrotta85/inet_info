//
// Copyright (C) 2000 Institut fuer Telematik, Universitaet Karlsruhe
// Copyright (C) 2004,2011 Andras Varga
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
// basic file operations
#include <iostream>
#include <fstream>

#include "inet/common/lifecycle/NodeOperations.h"
#include "inet/common/ModuleAccess.h"
#include "inet/common/packet/Packet.h"
#include "inet/common/TagBase_m.h"
#include "inet/common/TimeTag_m.h"
#include "inet/networklayer/common/L3AddressResolver.h"
#include "inet/transportlayer/contract/udp/UdpControlInfo_m.h"
#include "UdpBasicBeaconingApp.h"

namespace inet {

Define_Module(UdpBasicBeaconingApp);

UdpBasicBeaconingApp::~UdpBasicBeaconingApp()
{
    cancelAndDelete(selfMsg);
    cancelAndDelete(auto1sec);
}

void UdpBasicBeaconingApp::initialize(int stage)
{
    ApplicationBase::initialize(stage);

    if (stage == INITSTAGE_LOCAL) {
        numSent = 0;
        numReceived = 0;
        WATCH(numSent);
        WATCH(numReceived);

        localPort = par("localPort");
        destPort = par("destPort");
        startTime = par("startTime");
        stopTime = par("stopTime");
        packetName = par("packetName");

        neghOldTime = par("neghOldTime");
        cacheTimeout = par("cacheTimeout");

        myAppAddr = this->getParentModule()->getIndex();
        myIPAddr = Ipv4Address::UNSPECIFIED_ADDRESS;

        auto1sec = new cMessage("UdpBasicBeaconingApp1Sec");

        gc = check_and_cast<GeneralCache *>(this->getParentModule()->getParentModule()->getSubmodule("cache"));
        mob = check_and_cast<IMobility *>(this->getParentModule()->getSubmodule("mobility"));

        WATCH_VECTOR(addressTable);
        WATCH_MAP(neighMap_extra);

        if (stopTime >= SIMTIME_ZERO && stopTime < startTime)
            throw cRuntimeError("Invalid startTime/stopTime parameters");
        selfMsg = new cMessage("sendTimer");
    }
    else if ( stage == INITSTAGE_LAST) {
        addressTable.resize(this->getParentModule()->getVectorSize(), Ipv4Address::UNSPECIFIED_ADDRESS);
    }
}

void UdpBasicBeaconingApp::finish()
{
    recordScalar("packets sent", numSent);
    recordScalar("packets received", numReceived);
    ApplicationBase::finish();
}

void UdpBasicBeaconingApp::setSocketOptions()
{
    int timeToLive = par("timeToLive");
    if (timeToLive != -1)
        socket.setTimeToLive(timeToLive);

    int typeOfService = par("typeOfService");
    if (typeOfService != -1)
        socket.setTypeOfService(typeOfService);

    const char *multicastInterface = par("multicastInterface");
    if (multicastInterface[0]) {
        IInterfaceTable *ift = getModuleFromPar<IInterfaceTable>(par("interfaceTableModule"), this);
        InterfaceEntry *ie = ift->getInterfaceByName(multicastInterface);
        if (!ie)
            throw cRuntimeError("Wrong multicastInterface setting: no interface named \"%s\"", multicastInterface);
        socket.setMulticastOutputInterface(ie->getInterfaceId());
    }

    bool receiveBroadcast = par("receiveBroadcast");
    if (receiveBroadcast)
        socket.setBroadcast(true);

    bool joinLocalMulticastGroups = par("joinLocalMulticastGroups");
    if (joinLocalMulticastGroups) {
        MulticastGroupList mgl = getModuleFromPar<IInterfaceTable>(par("interfaceTableModule"), this)->collectMulticastGroups();
        socket.joinLocalMulticastGroups(mgl);
    }
}

L3Address UdpBasicBeaconingApp::chooseDestAddr()
{
    int k = intrand(destAddresses.size());
    if (destAddresses[k].isUnspecified() || destAddresses[k].isLinkLocal()) {
        L3AddressResolver().tryResolve(destAddressStr[k].c_str(), destAddresses[k]);
    }
    return destAddresses[k];
}

Packet *UdpBasicBeaconingApp::createPacket()
{
    std::ostringstream str;
    double n_snir, n_pow, n_per;

    long msgByteLength = (sizeof(struct node_neigh_info_t) * neighMap.size()) + (2 * sizeof(uint32_t));
    //long msgByteLength = par("messageLength");

    str << packetName << "-" << myAppAddr << "-" << numSent;
    Packet *pk = new Packet(str.str().c_str());
    const auto& payload = makeShared<ApplicationBaseBeacon>();
    payload->setChunkLength(B(msgByteLength));
    payload->setSequenceNumber(numSent);

    //add custom data
    payload->setSrc_appAddr(myAppAddr);
    payload->setNeighArraySize(neighMap.size());
    int k = 0;
    for (auto& n : neighMap){
        neigh_info_t *ni_p = &(*n.second.begin());
        node_neigh_info_t neigh_info;

        neigh_info.node_appAddr = ni_p->appAddr;
        calculatePhyStats(n_snir, n_pow, n_per, 3, ni_p->appAddr, 3);
        neigh_info.rcvPow_dBm = watt2dBm(n_pow);

        //neigh_info.estimatedDistance = neighMap_extra[ni_p->appAddr].estimatedPos.distance(mob->getCurrentPosition());
        neigh_info.estimatedDistance = neighMap_extra[ni_p->appAddr].estimatedDist;

        payload->setNeigh(k, neigh_info);
        ++k;
    }

    auto creationTimeTag = payload->addTag<CreationTimeTag>();
    creationTimeTag->setCreationTime(simTime());
    pk->insertAtEnd(payload);
    pk->addPar("sourceId") = getId();
    pk->addPar("msgId") = numSent;

    return pk;
}

void UdpBasicBeaconingApp::sendPacket()
{
    Packet *packet = createPacket();
    //L3Address destAddr = chooseDestAddr();
    L3Address destAddr = Ipv4Address::ALLONES_ADDRESS;
    emit(sentPkSignal, packet);
    socket.sendTo(packet, destAddr, destPort);
    numSent++;
}

void UdpBasicBeaconingApp::processStart()
{
    socket.setOutputGate(gate("socketOut"));
    const char *localAddress = par("localAddress");
    //socket.bind(*localAddress ? L3AddressResolver().resolve(localAddress) : L3Address(), localPort);
    socket.bind(localPort);
    setSocketOptions();

    const char *destAddrs = par("destAddresses");
    cStringTokenizer tokenizer(destAddrs);
    const char *token;

    while ((token = tokenizer.nextToken()) != nullptr) {
        destAddressStr.push_back(token);
        L3Address result;
        L3AddressResolver().tryResolve(token, result);
        if (result.isUnspecified())
            EV_ERROR << "cannot resolve destination address: " << token << endl;
        destAddresses.push_back(result);
    }

    IInterfaceTable *ift = getModuleFromPar<IInterfaceTable>(par("interfaceTableModule"), this);
    if (ift) {
        for (int i = 0; i < (int)addressTable.size(); i++) {
            if (i != myAppAddr) {
                char buf[32];
                snprintf(buf, sizeof(buf), "%s.host[%d]",this->getParentModule()->getParentModule()->getName(), i);
                L3Address addr = L3AddressResolver().resolve(buf);
                addressTable[i] = addr.toIPv4();
            }
        }

        if (myIPAddr == Ipv4Address::UNSPECIFIED_ADDRESS) {
            InterfaceEntry *wlan = ift->getInterfaceByName("wlan0");
            if (wlan) {
                myIPAddr = wlan->getIPv4Address();
                addressTable[myAppAddr] = myIPAddr;
            }
        }
    }

    if (!destAddresses.empty()) {
        selfMsg->setKind(SEND);
        processSend();
    }
    else {
        if (stopTime >= SIMTIME_ZERO) {
            selfMsg->setKind(STOP);
            scheduleAt(stopTime, selfMsg);
        }
    }
}

void UdpBasicBeaconingApp::processSend()
{
    sendPacket();
    simtime_t d = simTime() + par("sendInterval");
    if (stopTime < SIMTIME_ZERO || d < stopTime) {
        selfMsg->setKind(SEND);
        scheduleAt(d, selfMsg);
    }
    else {
        selfMsg->setKind(STOP);
        scheduleAt(stopTime, selfMsg);
    }
}

void UdpBasicBeaconingApp::processStop()
{
    socket.close();
}

void UdpBasicBeaconingApp::handleMessageWhenUp(cMessage *msg)
{
    if (msg == auto1sec) {
        manage_auto1sec();
        scheduleAt(simTime() + 1.0, auto1sec);
    }
    else if (msg->isSelfMessage()) {
        ASSERT(msg == selfMsg);
        switch (selfMsg->getKind()) {
            case START:
                processStart();
                scheduleAt(simTime() + 1.0, auto1sec);
                break;

            case SEND:
                processSend();
                break;

            case STOP:
                processStop();
                break;

            default:
                throw cRuntimeError("Invalid kind %d in self message", (int)selfMsg->getKind());
        }
    }
    else if (msg->getKind() == UDP_I_DATA) {
        // process incoming packet
        processPacket(PK(msg));
    }
    else if (msg->getKind() == UDP_I_ERROR) {
        EV_WARN << "Ignoring UDP error report\n";
        delete msg;
    }
    else {
        throw cRuntimeError("Unrecognized message (%s)%s", msg->getClassName(), msg->getName());
    }
}

void UdpBasicBeaconingApp::refreshDisplay() const
{
    char buf[100];
    sprintf(buf, "rcvd: %d pks\nsent: %d pks", numReceived, numSent);
    getDisplayString().setTagArg("t", 0, buf);
}

void UdpBasicBeaconingApp::processPacket(cPacket *pk)
{
    emit(rcvdPkSignal, pk);
    EV_INFO << "Received packet: " << UdpSocket::getReceivedPacketInfo(pk) << endl;

    if (strncmp(pk->getName(), packetName, strlen(packetName)) == 0) {
        manageReceivedBeacon(check_and_cast<Packet *>(pk));
    }

    delete pk;
    numReceived++;
}

bool UdpBasicBeaconingApp::handleNodeStart(IDoneCallback *doneCallback)
{
    simtime_t start = std::max(startTime, simTime());
    if ((stopTime < SIMTIME_ZERO) || (start < stopTime) || (start == stopTime && startTime == stopTime)) {
        selfMsg->setKind(START);
        scheduleAt(start, selfMsg);
    }
    return true;
}

bool UdpBasicBeaconingApp::handleNodeShutdown(IDoneCallback *doneCallback)
{
    if (selfMsg)
        cancelEvent(selfMsg);
    //TODO if(socket.isOpened()) socket.close();
    return true;
}

void UdpBasicBeaconingApp::handleNodeCrash()
{
    if (selfMsg)
        cancelEvent(selfMsg);
}

void UdpBasicBeaconingApp::calculatePhyStats(double &snir, double &pow, double &per, int data1_control2_all3, int nodeAdd, double sec) {
    double sum_snir, sum_pow, sum_per, count_w;
    std::list<GeneralCache::RecordElement> *pi_l;

    snir = pow = per = sum_snir = sum_pow = sum_per = count_w = 0;

    pi_l = gc->getList(myAppAddr, GeneralCache::PHYISICAL);
    if (pi_l != nullptr) {

        for (auto& pi_s : *pi_l) {
            simtime_t oldness = simTime() - pi_s.time;
            if (oldness <= sec) {
                Physical_Info *pi = static_cast<Physical_Info *>(pi_s.info);
                if ((nodeAdd < 0) || (nodeAdd == pi->txAddr)) {
                    if (    ((data1_control2_all3 == 1) && pi->isData) ||
                            ((data1_control2_all3 == 2) && (!pi->isData)) ||
                            (data1_control2_all3 == 3)  ) {
                        double act_count = (sec - oldness.dbl()) / sec;
                        act_count = std::pow(act_count, 2.0);

                        sum_snir += pi->snir * act_count;
                        sum_per += pi->per * act_count;
                        sum_pow += pi->pow.get() * act_count;
                        count_w += act_count;
                    }
                }
            }
            else {
                // the list is time-ordered
                break;
            }
        }
    }

    if (count_w > 0) {
        snir = sum_snir / count_w;
        per = sum_per / count_w;
        pow = sum_pow / count_w;
    }
    else {
        snir = -1;
        per = -1;
        pow = -1;
    }
}

void UdpBasicBeaconingApp::manageReceivedBeacon(Packet *pk) {
    const auto& appmsg = pk->peekDataAt<ApplicationBaseBeacon>(B(0), B(pk->getByteLength()));
    if (!appmsg)
        throw cRuntimeError("Message (%s)%s is not a ApplicationBaseBeacon -- probably wrong client app, or wrong setting of UDP's parameters", pk->getClassName(), pk->getName());

    int rcvAppAddr = appmsg->getSrc_appAddr();
    if (rcvAppAddr != myAppAddr){
        if (neighMap.count(rcvAppAddr) == 0) {
            std::list<neigh_info_t> *nList = new std::list<neigh_info_t>();
            neighMap[rcvAppAddr] = *nList;

            neigh_extra_info_t rcvExtraInfo;
            rcvExtraInfo.appAddr = rcvAppAddr;
            rcvExtraInfo.estimatedPos = Coord::NIL;
            neighMap_extra[rcvAppAddr] = rcvExtraInfo;
        }
        neigh_info_t rcvInfo;
        rcvInfo.timestamp_lastSeen = simTime();
        rcvInfo.appAddr = rcvAppAddr;
        rcvInfo.lastSequenceNumber = appmsg->getSequenceNumber();
        rcvInfo.neigh_info.resize(appmsg->getNeighArraySize());
        for (int i = 0; i < appmsg->getNeighArraySize(); i++){
            rcvInfo.neigh_info[i] = appmsg->getNeigh(i);
        }

        neighMap[rcvAppAddr].push_front(rcvInfo);

        updateDistEstimation(rcvAppAddr);
    }
    updatePosEstimation();
}

void UdpBasicBeaconingApp::manage_auto1sec(void) {
    gc->removeOldCache(simTime(), cacheTimeout.dbl());
    updateNeighMap();
}

void UdpBasicBeaconingApp::updateNeighMap(void) {
    bool updated;
    simtime_t now = simTime();

    do {
        updated = false;
        for (auto it = neighMap.begin(); it != neighMap.end(); it++) {
            if ((now - it->second.begin()->timestamp_lastSeen) > neghOldTime) {

                neighMap_extra.erase(it->first);

                it->second.clear();
                neighMap.erase(it);
                updated = true;
                break;
            }
            else {
                std::list<neigh_info_t>::iterator itList = it->second.begin();
                while (itList != it->second.end()) {
                    if ((now - itList->timestamp_lastSeen).dbl() > neghOldTime.dbl()) {
                        itList = it->second.erase(itList);
                    }
                    else {
                        ++itList;
                    }
                }
            }
        }

    } while (updated == true);
}

void UdpBasicBeaconingApp::updateDistEstimation(int nodeAppAddr) {
    if (neighMap_extra.count(nodeAppAddr) > 0) {
        Coord estimatedPosition = Coord::ZERO;

        // CHEATING
        IMobility *neighMob = check_and_cast<IMobility *>(this->getParentModule()->getParentModule()->getSubmodule("host", nodeAppAddr)->getSubmodule("mobility"));
        estimatedPosition = neighMob->getCurrentPosition();

        neighMap_extra[nodeAppAddr].estimatedDist = estimatedPosition.distance(mob->getCurrentPosition());
    }
}


void UdpBasicBeaconingApp::updatePosEstimation(void) {
    std::map<int, std::map<int, NodeLocalizatorBasic::matrix_info_t> > matrixMap;
    std::map<int, std::map<int, NodeLocalizatorBasic::matrix_info_t> > simmetricMatrixMap;
    std::map< int, Coord > estimatedPositions;

    //NodeLocalizatorBasic *nodeLocalizator = new NodeLocalizatorSweep();
    NodeLocalizatorBasic *nodeLocalizator = new NodeLocalizatorSpring();

    for (auto &n : neighMap) {
        if (neighMap_extra.count(n.first) == 0) {
            throw cRuntimeError("updatePosEstimation: neighMap and neighMap_extra are not coordinated");
        }
        estimatedPositions[n.first] = Coord::NIL;
    }
    for (auto &n : neighMap) {
        for (auto &nn : n.second.begin()->neigh_info) {
            if (estimatedPositions.count(nn.node_appAddr) == 0){
                estimatedPositions[nn.node_appAddr] = Coord::NIL;
            }
        }
    }

    for (auto& e1 : estimatedPositions) {
        std::map<int, NodeLocalizatorBasic::matrix_info_t> *newMap = new std::map<int, NodeLocalizatorBasic::matrix_info_t> ();
        matrixMap[e1.first] = *newMap;
        for (auto& e2 : estimatedPositions) {
            NodeLocalizatorBasic::matrix_info_t newms;
            if (e1.first == e2.first){
                newms.declaredDistance = 0;
            }
            else {
                newms.declaredDistance = std::numeric_limits<double>::max();
            }
            matrixMap[e1.first][e2.first] = newms;
        }
    }

    for (auto &n : neighMap) {
        matrixMap[myAppAddr][n.first].declaredDistance = neighMap_extra[n.first].estimatedDist;

        for (auto &nn : n.second.begin()->neigh_info) {
            matrixMap[n.first][nn.node_appAddr].declaredDistance = nn.estimatedDistance;
        }
    }

    for (auto& m1 : matrixMap) {
        std::map<int, NodeLocalizatorBasic::matrix_info_t> *newMap = new std::map<int, NodeLocalizatorBasic::matrix_info_t> ();
        simmetricMatrixMap[m1.first] = *newMap;
        for (auto& m2 : matrixMap[m1.first]) {
            NodeLocalizatorBasic::matrix_info_t newms;

            if (matrixMap[m1.first][m2.first].declaredDistance == std::numeric_limits<double>::max()) {
                newms.declaredDistance = matrixMap[m2.first][m1.first].declaredDistance;
            }
            else if (matrixMap[m2.first][m1.first].declaredDistance == std::numeric_limits<double>::max()) {
                newms.declaredDistance = matrixMap[m1.first][m2.first].declaredDistance;
            }
            else {
                newms.declaredDistance = (matrixMap[m1.first][m2.first].declaredDistance + matrixMap[m2.first][m1.first].declaredDistance) / 2.0;
            }
            simmetricMatrixMap[m1.first][m2.first] = newms;
        }
    }

    nodeLocalizator->estimatePos(estimatedPositions, matrixMap, simmetricMatrixMap);

    if (myAppAddr == 0) {
        debugFilePrint(estimatedPositions);
    }
}


bool compare_pair (const std::pair<int, double>& firstP, const std::pair<int, double>& secondP) {
  return ( firstP.second < secondP.second );
}

void UdpBasicBeaconingApp::updatePosEstimation_old(void) {
    std::map<int, std::map<int, matrix_spring_t> > matrixMap;
    std::map< int, Coord > estimatedPositions;

    //estimatedPositions[myAppAddr] = Coord::ZERO;

    // init the direct link neigh
    for (auto &n : neighMap) {
        if (neighMap_extra.count(n.first) == 0) {
            throw cRuntimeError("updatePosEstimation: neighMap and neighMap_extra are not coordinated");
        }

        //if (myAppAddr == 0){
        //    std::cout << "Actual estimatedPositions for " << n.first << " is: " << neighMap_extra[n.first].estimatedPos << std::endl << std::flush;
        //    std::cout << "Actual estimatedPositions is NIL? " << (neighMap_extra[n.first].estimatedPos.isUnspecified()) << std::endl << std::endl << std::flush;
        //}

        if (neighMap_extra[n.first].estimatedPos.isUnspecified()) {
            double x_R, y_R;
            //double l = (neighMap_extra[n.first].estimatedDist + n.second) / 2.0;
            double my_l = neighMap_extra[n.first].estimatedDist;

            double his_l = my_l;
            for (auto &nn : n.second.begin()->neigh_info) {
                if (nn.node_appAddr == myAppAddr){
                    his_l = nn.estimatedDistance;
                    break;
                }
            }

            double l = (my_l + his_l) / 2.0;
            double t = (dblrand() * (2.0 * M_PI)) - M_PI;
            polar2cartesian(t, l, x_R, y_R);

            estimatedPositions[n.first] = Coord(x_R, y_R);

            //if (myAppAddr == 0){
            //    std::cout << "estimatedPositions for " << n.first << " is: " << estimatedPositions[n.first] << std::endl << std::flush;
            //}
        }
        else {
            estimatedPositions[n.first] = neighMap_extra[n.first].estimatedPos;
        }

        estimatedPositions[n.first] = Coord::NIL;
    }
    // init the undirect link neigh
    for (auto &n : neighMap) {
        for (auto &nn : n.second.begin()->neigh_info) {
            if (estimatedPositions.count(nn.node_appAddr) == 0){
                if (neighMap_extra[nn.node_appAddr].estimatedPos.isUnspecified()) {
                    double x_R, y_R, tn, ln;

                    cartesian2polar(estimatedPositions[n.first].x, estimatedPositions[n.first].y, tn, ln);

                    double t = tn;
                    double l = ln + nn.estimatedDistance;

                    polar2cartesian(t, l, x_R, y_R);
                    estimatedPositions[nn.node_appAddr] = Coord(x_R, y_R);
                }
                else {
                    estimatedPositions[nn.node_appAddr] = neighMap_extra[nn.node_appAddr].estimatedPos;
                }

                estimatedPositions[nn.node_appAddr] = Coord::NIL;
            }
        }
    }

    /*******************************************************************************************************/
    for (auto& e1 : estimatedPositions) {
        std::map<int, matrix_spring_t> *newMap = new std::map<int, matrix_spring_t> ();
        matrixMap[e1.first] = *newMap;
        for (auto& e2 : estimatedPositions) {
            matrix_spring_t newms;
            if (e1.first == e2.first){
                newms.declaredDistance = 0;
            }
            else {
                newms.declaredDistance = std::numeric_limits<double>::max();
            }
            matrixMap[e1.first][e2.first] = newms;
        }
    }

    for (auto &n : neighMap) {
        matrixMap[myAppAddr][n.first].declaredDistance = neighMap_extra[n.first].estimatedDist;

        for (auto &nn : n.second.begin()->neigh_info) {
            matrixMap[n.first][nn.node_appAddr].declaredDistance = nn.estimatedDistance;
        }
    }

    // DEBUG PRINT
    /*for (auto& mm1 : matrixMap) {
        std::cout << mm1.first << "\t";
        for (auto& mm2 : mm1.second) {
            std::cout << mm2.first << ":" << mm2.second.declaredDistance << "\t";
            if (mm1.first == mm2.first) {
                std::cout << "\t";
            }
        }
        std::cout << endl;
    }*/

    //estimatedPositions[myAppAddr] = Coord::ZERO;
    std::list< std::pair<int, double> > myNeigList;
    for (auto& mn: matrixMap[myAppAddr]) {
        if ((mn.first != myAppAddr) && (mn.second.declaredDistance < std::numeric_limits<double>::max())){
            double dist = mn.second.declaredDistance;
            if (matrixMap[mn.first][myAppAddr].declaredDistance < std::numeric_limits<double>::max()){
                dist += matrixMap[mn.first][myAppAddr].declaredDistance;
                dist /= 2.0;
            }
            myNeigList.push_back(std::make_pair(mn.first, dist));
        }
    }
    myNeigList.sort(compare_pair);

    // DEBUG PRINT
    /*std::cout << myAppAddr << " - MY NEIGH: ";
    for (auto& vl : myNeigList) {
        std::cout << vl.first << ":" << vl.second << "\t";
    }
    std::cout << endl << endl;*/

    estimatePos_phase1(estimatedPositions, matrixMap, myNeigList);

    if (myAppAddr == 0) {
        debugFilePrint(estimatedPositions);
    }
}

void UdpBasicBeaconingApp::estimatePos_phase1_1statt(std::map< int, Coord > &estPos, std::map<int, std::map<int, matrix_spring_t> > &distMMap, std::list< std::pair<int, double> > &myNeigList) {
    std::list<int> okList;
    std::list< std::pair<int, double> > neigList;
    for (auto& cl : myNeigList) {
        neigList.push_back(std::make_pair(cl.first, cl.second));
    }

    std::map<int, std::map<int, matrix_spring_t> > simmetricDistMMap;
    for (auto& m1 : distMMap) {
        std::map<int, matrix_spring_t> *newMap = new std::map<int, matrix_spring_t> ();
        simmetricDistMMap[m1.first] = *newMap;
        for (auto& m2 : distMMap[m1.first]) {
            matrix_spring_t newms;

            if (distMMap[m1.first][m2.first].declaredDistance == std::numeric_limits<double>::max()) {
                newms.declaredDistance = distMMap[m2.first][m1.first].declaredDistance;
            }
            else if (distMMap[m2.first][m1.first].declaredDistance == std::numeric_limits<double>::max()) {
                newms.declaredDistance = distMMap[m1.first][m2.first].declaredDistance;
            }
            else {
                newms.declaredDistance = (distMMap[m1.first][m2.first].declaredDistance + distMMap[m2.first][m1.first].declaredDistance) / 2.0;
            }
            simmetricDistMMap[m1.first][m2.first] = newms;
        }
    }

    // setting the first three nodes
    estPos[myAppAddr] = Coord::ZERO;
    okList.push_back(myAppAddr);

    // looking for three connected nodes (me included)
    std::list< std::pair<int, double> >::iterator itN = neigList.begin();
    int n1, n2;
    n1 = n2 = -1;
    while (itN != neigList.end()) {
        std::list< std::pair<int, double> >::iterator itN2 = neigList.begin();
        //itN2 = itN;
        //itN2++;
        while (itN2 != neigList.end()) {
            if (itN->first != itN2->first) {
                if (simmetricDistMMap[itN->first][itN2->first].declaredDistance < std::numeric_limits<double>::max()) {
                    double ab, bc, ac, alpha;

                    // ok, i found them
                    n1 = itN->first;
                    n2 = itN2->first;

                    ab = simmetricDistMMap[myAppAddr][itN->first].declaredDistance;
                    bc = simmetricDistMMap[itN->first][itN2->first].declaredDistance;
                    ac = simmetricDistMMap[myAppAddr][itN2->first].declaredDistance;
                    alpha = ((ab*ab) + (ac*ac) - (bc*bc))/(2.0 * ab * ac);

                    // check if They are not on the same straight line
                    double angle = acos(((ab*ab) + (bc*bc) - (ac*ac)) / (2.0 * ab * bc));
                    if ((angle > (M_PI*0.1)) && (angle < (M_PI*0.9))) {

                        // adding them
                        estPos[itN->first] = Coord(itN->second, 0.0);
                        estPos[itN2->first] = Coord(ac * alpha, ac * sqrt(1 - (alpha * alpha)));

                        okList.push_back(itN->first);
                        okList.push_back(itN2->first);

                        break;
                    }
                }
            }
            itN2++;
        }
        if ((n1 >= 0) && (n2 >= 0)) {
            break;
        }
        itN++;
    }
    //erase n1 and n2
    if ((n1 >= 0) && (n2 >= 0)) {
        std::list< std::pair<int, double> >::iterator itErase = neigList.begin();
        while (itErase != neigList.end()) {
            if ((itErase->first == n1) || (itErase->first == n2)) {
                itErase = neigList.erase(itErase);
            }
            else {
                ++itErase;
            }
        }
    }
    else {
        // not found three connected nodes... what to do?
        //TODO
    }

    if (neigList.size() > 0) {
        int nAddr, nNeigh;
        getnextToAdd(nAddr, nNeigh, neigList, okList, simmetricDistMMap);
        if (nNeigh >= 3) {

        }
    }

    if (neigList.size() > 0) {
        itN = neigList.begin();
        while (itN != neigList.end()) {
            if (true) {
                itN = neigList.erase(itN);
            }
            else {
                ++itN;
            }
        }
    }

/*

    if (itN != neigList.end()) {
        estPos[itN->first] = Coord(itN->second, 0.0);
        //itN = neigList.erase(itN);
        itN++;
    }

    if (itN != neigList.end()) {
        double ab, bc, ac;
        ab = simmetricDistMMap[myAppAddr][neigList.begin()->first].declaredDistance;
        bc = simmetricDistMMap[neigList.begin()->first][itN->first].declaredDistance;
        ac = simmetricDistMMap[myAppAddr][itN->first].declaredDistance;
        double alpha = ((ab*ab) + (ac*ac) - (bc*bc))/(2.0 * ab * ac);
        estPos[itN->first] = Coord(ac * alpha, ac * sqrt(1 - (alpha * alpha)));
        neigList.erase(itN);
    }
*/

}

void UdpBasicBeaconingApp::getnextToAdd(int &risAddr, int &numNeigh, std::list< std::pair<int, double> > &toAddList,
        std::list< int > &addedList, std::map<int, std::map<int, matrix_spring_t> > &simmetricDistMMap) {
    std::map<int, int> risMap;

    risAddr = numNeigh = -1;

    for(auto& nc : toAddList) {
        for(auto& na : addedList) {
            if (simmetricDistMMap[nc.first][na].declaredDistance  < std::numeric_limits<double>::max()) {
                if (risMap.count(nc.first) == 0) {
                    risMap[nc.first] = 0;
                }
                risMap[nc.first]++;
            }
        }
    }
    for(auto& nm : risMap) {
        if (nm.second > numNeigh) {
            numNeigh = nm.second;
            risAddr = nm.first;
        }
    }
}

void UdpBasicBeaconingApp::estimatePos_phase1(std::map< int, Coord > &estPos, std::map<int, std::map<int, matrix_spring_t> > &distMMap, std::list< std::pair<int, double> > &myNeigList) {
    std::list<int> okList;
    std::list<int> toAddList;

    std::list< std::pair<int, double> > neigList;
    for (auto& cl : myNeigList) {
        neigList.push_back(std::make_pair(cl.first, cl.second));
    }

    std::map<int, std::map<int, matrix_spring_t> > simmetricDistMMap;
    for (auto& m1 : distMMap) {
        std::map<int, matrix_spring_t> *newMap = new std::map<int, matrix_spring_t> ();
        simmetricDistMMap[m1.first] = *newMap;
        for (auto& m2 : distMMap[m1.first]) {
            matrix_spring_t newms;

            if (distMMap[m1.first][m2.first].declaredDistance == std::numeric_limits<double>::max()) {
                newms.declaredDistance = distMMap[m2.first][m1.first].declaredDistance;
            }
            else if (distMMap[m2.first][m1.first].declaredDistance == std::numeric_limits<double>::max()) {
                newms.declaredDistance = distMMap[m1.first][m2.first].declaredDistance;
            }
            else {
                newms.declaredDistance = (distMMap[m1.first][m2.first].declaredDistance + distMMap[m2.first][m1.first].declaredDistance) / 2.0;
            }
            simmetricDistMMap[m1.first][m2.first] = newms;
        }
    }

    for (auto& m : simmetricDistMMap) {
        toAddList.push_back(m.first);
    }
    int f1, f2, f3;
    findAndAddFirst3Node(f1, f2, f3, simmetricDistMMap, estPos);
    toAddList.remove(f1);
    toAddList.remove(f2);
    toAddList.remove(f3);
    okList.push_back(f1);
    okList.push_back(f2);
    okList.push_back(f3);

    executeSweep(okList, toAddList, simmetricDistMMap, estPos);
}

void UdpBasicBeaconingApp::executeSweep(std::list<int> &okList, std::list<int> &toAddList, std::map<int, std::map<int, matrix_spring_t> > &simmetricDistMMap, std::map< int, Coord > &estPos) {
    if (okList.size() == 3) {
        std::list<sweep_node_t> orderL_new;
        std::list<sweep_node_t> okList_new;
        std::list<sweep_node_t> toAddList_new;
        for (auto& ok : okList) {
            sweep_node_t nnn;
            nnn.addr = ok;
            if (estPos.count(nnn.addr) != 0) {
                sweep_position_t np;
                np.pos = estPos[nnn.addr];
                nnn.positions.push_back(np);
            }
            okList_new.push_back(nnn);
        }
        for (auto& to : toAddList) {
            sweep_node_t nnn;
            nnn.addr = to;
            toAddList_new.push_back(nnn);
        }

        order(orderL_new, okList_new, toAddList_new, simmetricDistMMap);

        //std::list<int> orderL;
        //order(orderL, okList, toAddList, simmetricDistMMap);


        std::cout << myAppAddr << " - OREDR-LIST:" ;
        for (auto& n : orderL_new) {
            std::cout << " [" << n.addr << "-";
            for (auto& pp : n.positions) {
                std::cout << pp.pos;
            }
            std::cout << "]";
        }
        std::cout << endl;


        for (auto& o : orderL_new) {
            if (estPos.count(o.addr) == 0){     // check this
                std::list<sweep_node_t> ancestors;
                for (auto& anc : orderL_new) {
                    if (anc.addr == o.addr) break;
                    ancestors.push_back(anc);
                }

                sweep_node_t a1, a2;
                auto ita = ancestors.begin();
                if (ita != ancestors.end()) {
                    a1 = *ita;
                    ita++;
                }
                if (ita != ancestors.end()) {
                    a2 = *ita;
                    ita++;
                }

                bilaterate (&o, &a1, &a2, simmetricDistMMap);
                while (ita != ancestors.end()) {
                    update_bilateration (&o, &(*ita), simmetricDistMMap);
                    ita++;
                }
            }
        }
    }
}

void UdpBasicBeaconingApp::bilaterate (sweep_node_t *u, sweep_node_t *v, sweep_node_t *w, std::map<int, std::map<int, matrix_spring_t> > &simmetricDistMMap) {
    std::list<sweep_position_t> newPu;
    for (auto& pv : v->positions) {
        for (auto& pw : w->positions) {
            if (consistent(&pv, &pw)) {
                sweep_position_t pu1, pu2;
                circleIntersection(pu1, pu2, pv, simmetricDistMMap[v->addr][u->addr].declaredDistance, pw, simmetricDistMMap[w->addr][u->addr].declaredDistance);
                //TODO check pu1, pu2

                for (auto& pve : pv.anchestorsByDepth){
                    pu1.anchestorsByDepth.push_back(pve);
                    pu2.anchestorsByDepth.push_back(pve);
                }
                for (auto& pwe : pw.anchestorsByDepth){
                    pu1.anchestorsByDepth.push_back(pwe);
                    pu2.anchestorsByDepth.push_back(pwe);
                }
                pu1.anchestorsByDepth.push_back(pv.pos);
                pu1.anchestorsByDepth.push_back(pw.pos);
                pu2.anchestorsByDepth.push_back(pv.pos);
                pu2.anchestorsByDepth.push_back(pw.pos);


                newPu.push_back(pu1);
                newPu.push_back(pu2);
            }
        }
    }
    for (auto& pp : newPu) {
        u->positions.push_back(pp);
    }
}

void UdpBasicBeaconingApp::update_bilateration (sweep_node_t *u, sweep_node_t *v, std::map<int, std::map<int, matrix_spring_t> > &simmetricDistMMap) {
    for (auto& pu : u->positions) {
        for (auto& pv : v->positions) {
            if (consistent(&pu, &pv)) {

            }
        }
    }
}

bool UdpBasicBeaconingApp::consistent (sweep_position_t *u, sweep_position_t *v) {
    return true;
}

void UdpBasicBeaconingApp::circleIntersection (sweep_position_t &pu1, sweep_position_t &pu2, sweep_position_t &pv, double duv, sweep_position_t &pw, double duw) {
    double r, R, d, dx, dy, cx, cy, Cx, Cy;

    pu1.pos = pu2.pos = Coord::NIL;

    if (duv < duw) {
        r  = duv;  R = duw;
        cx = pv.pos.x; cy = pv.pos.y;
        Cx = pw.pos.x; Cy = pw.pos.y;
    } else {
        r  = duw; R  = duv;
        Cx = pv.pos.x; Cy = pv.pos.y;
        cx = pw.pos.x; cy = pw.pos.y;
    }

    // Compute the vector <dx, dy>
    dx = cx - Cx;
    dy = cy - Cy;

    // Find the distance between two points.
    d = sqrt( dx*dx + dy*dy );

    // There are an infinite number of solutions
    // Seems appropriate to also return null
    if ((d < EPSILON) && (abs(R-r) < EPSILON)) {
        return;
    }
    // No intersection (circles centered at the
    // same place with different size)
    else if (d < EPSILON) {
        return;
    }

    double x = (dx / d) * R + Cx;
    double y = (dy / d) * R + Cy;
    Coord P = Coord(x, y);

    // Single intersection (kissing circles)
    if ((abs((R+r)-d) < EPSILON) || (abs(R-(r+d)) < EPSILON)) {
        pu1.pos = P;
        return;
    }

    // No intersection. Either the small circle contained within
    // big circle or circles are simply disjoint.
    if ( ((d+r) < R) || ((R+r < d)) ) {
        return;
    }

    Coord C = Coord(Cx, Cy);
    double angle = acossafe((r*r-d*d-R*R)/(-2.0*d*R));
    pu1.pos = rotatePoint(C, P, +angle);
    pu2.pos = rotatePoint(C, P, -angle);
}

void UdpBasicBeaconingApp::order(std::list<sweep_node_t> &orderL, std::list<sweep_node_t> &okList, std::list<sweep_node_t> &toAddList, std::map<int, std::map<int, matrix_spring_t> > &simmetricDistMMap) {
    std::list<sweep_node_t> toAddList_copy;

    orderL.clear();
    for (auto& n : okList) {
        orderL.push_back(n);
    }

    for (auto& n : toAddList) {
        toAddList_copy.push_back(n);
    }

    std::list<sweep_node_t> shell;
    do {
        shell.clear();

        for (auto& x : toAddList_copy) {
            int nn = 0;
            for (auto& n : orderL) {
                if (simmetricDistMMap[x.addr][n.addr].declaredDistance < std::numeric_limits<double>::max()) {
                    ++nn;
                }
            }
            if (nn >= 2) {
                shell.push_back(x);
            }
        }
        for (auto& x : shell) {
            orderL.push_back(x);
            //toAddList_copy.remove(x);
            for(auto itt = toAddList_copy.begin(); itt != toAddList_copy.end(); itt++) {
                if (itt->addr == x.addr) {
                    toAddList_copy.erase(itt);
                    break;
                }
            }
        }

    } while(shell.size() > 0);
}

#define BEST_ANGLE 60.0
#define BEST_ANGLE_SIGMA 30
#define BEST_ANGLE_MULT 3.0

#define BEST_DISTANCE 5.0
#define BEST_DISTANCE_SIGMA 50.0
#define BEST_DISTANCE_MULT 1.0

void UdpBasicBeaconingApp::findAndAddFirst3Node(int &f1, int &f2, int &f3, std::map<int, std::map<int, matrix_spring_t> > &distMMap, std::map< int, Coord > &estPos) {
    double maxValue = 0;
    for (auto& m1 : distMMap) {
        for (auto& m2 : distMMap){
            for (auto& m3 : distMMap){
                if ((m1.first != m2.first) && (m1.first != m3.first) && (m2.first != m3.first)) {
                    double ab, bc, ac;
                    double abc, bca, cab;
                    double actValue = 0;

                    ab = distMMap[m1.first][m2.first].declaredDistance;
                    bc = distMMap[m2.first][m3.first].declaredDistance;
                    ac = distMMap[m1.first][m3.first].declaredDistance;

                    if (    (ab < std::numeric_limits<double>::max()) &&
                            (bc < std::numeric_limits<double>::max()) &&
                            (ac < std::numeric_limits<double>::max()) ) {

                        abc = acos(((ab*ab) + (bc*bc) - (ac*ac)) / (2.0 * ab * bc));
                        bca = acos(((bc*bc) + (ac*ac) - (ab*ab)) / (2.0 * bc * ac));
                        cab = acos(((ac*ac) + (ab*ab) - (bc*bc)) / (2.0 * ac * ab));

                        /*
                        actValue += BEST_ANGLE_MULT * (1.0/(BEST_ANGLE_SIGMA*sqrt(2.0*M_PI))) * exp( -0.5 * ( pow( (abc - BEST_ANGLE) / BEST_ANGLE_SIGMA, 2.0)) );
                        actValue += BEST_ANGLE_MULT * (1.0/(BEST_ANGLE_SIGMA*sqrt(2.0*M_PI))) * exp( -0.5 * ( pow( (bca - BEST_ANGLE) / BEST_ANGLE_SIGMA, 2.0)) );
                        actValue += BEST_ANGLE_MULT * (1.0/(BEST_ANGLE_SIGMA*sqrt(2.0*M_PI))) * exp( -0.5 * ( pow( (cab - BEST_ANGLE) / BEST_ANGLE_SIGMA, 2.0)) );

                        actValue += BEST_DISTANCE_MULT * (1.0/(BEST_DISTANCE_SIGMA*sqrt(2.0*M_PI))) * exp( -0.5 * ( pow( (ab - BEST_DISTANCE) / BEST_DISTANCE_SIGMA, 2.0)) );
                        actValue += BEST_DISTANCE_MULT * (1.0/(BEST_DISTANCE_SIGMA*sqrt(2.0*M_PI))) * exp( -0.5 * ( pow( (bc - BEST_DISTANCE) / BEST_DISTANCE_SIGMA, 2.0)) );
                        actValue += BEST_DISTANCE_MULT * (1.0/(BEST_DISTANCE_SIGMA*sqrt(2.0*M_PI))) * exp( -0.5 * ( pow( (ac - BEST_DISTANCE) / BEST_DISTANCE_SIGMA, 2.0)) );
                         */

                        actValue += BEST_ANGLE_MULT * exp( -0.5 * ( pow( (abc - BEST_ANGLE) / BEST_ANGLE_SIGMA, 2.0)) );
                        actValue += BEST_ANGLE_MULT * exp( -0.5 * ( pow( (bca - BEST_ANGLE) / BEST_ANGLE_SIGMA, 2.0)) );
                        actValue += BEST_ANGLE_MULT * exp( -0.5 * ( pow( (cab - BEST_ANGLE) / BEST_ANGLE_SIGMA, 2.0)) );

                        actValue += BEST_DISTANCE_MULT * exp( -0.5 * ( pow( (ab - BEST_DISTANCE) / BEST_DISTANCE_SIGMA, 2.0)) );
                        actValue += BEST_DISTANCE_MULT * exp( -0.5 * ( pow( (bc - BEST_DISTANCE) / BEST_DISTANCE_SIGMA, 2.0)) );
                        actValue += BEST_DISTANCE_MULT * exp( -0.5 * ( pow( (ac - BEST_DISTANCE) / BEST_DISTANCE_SIGMA, 2.0)) );

                        if (actValue > maxValue) {
                            maxValue = actValue;
                            f1 = m1.first;
                            f2 = m2.first;
                            f3 = m3.first;
                        }
                    }
                }
            }
        }
    }

    if (maxValue > 0) {
        double ab, bc, ac, alpha;

        ab = distMMap[f1][f2].declaredDistance;
        bc = distMMap[f2][f3].declaredDistance;
        ac = distMMap[f1][f3].declaredDistance;
        alpha = ((ab*ab) + (ac*ac) - (bc*bc))/(2.0 * ab * ac);

        estPos[f1] = Coord::ZERO;
        estPos[f2] = Coord(ab, 0.0);
        estPos[f3] = Coord(ac * alpha, ac * sqrt(1 - pow(alpha, 2.0)));
    }
}

void UdpBasicBeaconingApp::debugFilePrint(std::map< int, Coord > &mapC) {
    std::ofstream myfile;
    myfile.open ("estimatedPos.dot");
    double sizeReduction = 80.0;
    //myfile << "Writing this to a file.\n";

    if (myfile.is_open()) {
        myfile << "graph G {" << endl;

        for (auto& n: mapC) {
            if (!n.second.isUnspecified()) {
                myfile << "\t" << n.first << " [ pos = \"" << n.second.x/sizeReduction << "," << n.second.y/sizeReduction << "!\" ];" << endl;
            }
        }

        myfile << "}" << endl;
        myfile.close();
    }
}

} // namespace inet

