/*
 * NodeLocalizatorBasic.h
 *
 *  Created on: Apr 12, 2018
 *      Author: angelo
 */

#ifndef INET_APPLICATIONS_UDPAPP_NODELOCALIZATORBASIC_H_
#define INET_APPLICATIONS_UDPAPP_NODELOCALIZATORBASIC_H_

#include <vector>
#include <map>

#include "inet/common/INETDefs.h"
#include "inet/common/geometry/common/Coord.h"


class NodeLocalizatorBasic {
public:
    typedef struct {
        double declaredDistance;
    } matrix_info_t;

public:
    NodeLocalizatorBasic();
    virtual ~NodeLocalizatorBasic();

    virtual void estimatePos(std::map< int, inet::Coord > &estimatedPositions,
            std::map<int, std::map<int, matrix_info_t> > &matrixMapInfo,
            std::map<int, std::map<int, matrix_info_t> > &simmetricMatrixMapInfo);

    void findAndAddFirst3Node(std::list< std::pair<int, inet::Coord> > &ris3Points, std::map<int, std::map<int, matrix_info_t> > &distMMap);

    void circleIntersection (inet::Coord &pu1, inet::Coord &pu2, inet::Coord &pv, double duv, inet::Coord &pw, double duw);

public:

public:
    static double watt2dBm(double w) {
        return (10.0 * log10( w ) + 30.0);
    }

    static void polar2cartesian(double t, double l, double &x, double &y) {
        x = cos(t) * l;
        y = sin(t) * l;
    }

    static void cartesian2polar(double x, double y, double &t, double &l) {
        l = hypot(x, y);
        t = atan2(y, x);
    }

    // Due to double rounding precision the value passed into the Math.acos
    // function may be outside its domain of [-1, +1] which would return
    // the value NaN which we do not want.
    static double acossafe(double x) {
      if (x >= +1.0) return 0;
      if (x <= -1.0) return M_PI;
      return acos(x);
    }

    // Rotates a point about a fixed point at some angle 'a'
    static inet::Coord rotatePoint(inet::Coord fp, inet::Coord pt, double a) {
      double x = pt.x - fp.x;
      double y = pt.y - fp.y;
      double xRot = x * cos(a) + y * sin(a);
      double yRot = y * cos(a) - x * sin(a);
      return inet::Coord(fp.x+xRot, fp.y+yRot);
    }
};

#endif /* INET_APPLICATIONS_UDPAPP_NODELOCALIZATORBASIC_H_ */
