/*
 * NodeLocalizatorSweep.h
 *
 *  Created on: Apr 12, 2018
 *      Author: angelo
 */

#ifndef INET_APPLICATIONS_UDPAPP_NODELOCALIZATORSWEEP_H_
#define INET_APPLICATIONS_UDPAPP_NODELOCALIZATORSWEEP_H_

#include "NodeLocalizatorBasic.h"

class NodeLocalizatorSweep : public NodeLocalizatorBasic {
public:
    NodeLocalizatorSweep();
    virtual ~NodeLocalizatorSweep();

    virtual void estimatePos(std::map< int, inet::Coord > &estimatedPositions,
                std::map<int, std::map<int, matrix_info_t> > &matrixMapInfo,
                std::map<int, std::map<int, matrix_info_t> > &simmetricMatrixMapInfo);
};

#endif /* INET_APPLICATIONS_UDPAPP_NODELOCALIZATORSWEEP_H_ */
