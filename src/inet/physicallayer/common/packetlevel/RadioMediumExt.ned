//
// Copyright (C) 2013 OpenSim Ltd.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//

package inet.physicallayer.common.packetlevel;

import inet.physicallayer.common.packetlevel.RadioMedium;

//
// The medium model describes the shared physical medium where communication
// takes place. It keeps track of radios, noise sources, ongoing transmissions,
// background noise, and other ongoing noises. The medium computes when, where
// and how transmissions and noises arrive at receivers. It also efficiently
// provides the set of interfering transmissions and noises for the receivers.
//
module RadioMediumExt extends RadioMedium
{
    parameters:
        
        analogModelType = default("ScalarAnalogModel");
        backgroundNoiseType = default("IsotropicScalarBackgroundNoise");
        propagationType = default("ConstantSpeedPropagation");
        pathLossType = default("FreeSpacePathLoss");
        backgroundNoise.power = default(-110dBm);
        mediumLimitCache.carrierFrequency = default(2.4GHz);
        mediumLimitCache.minReceptionPower = default(-85dBm);
        mediumLimitCache.minInterferencePower = default(-110dBm);
        
        @class(RadioMediumExt);
        @display("i=misc/sun_s");

        @signal[radioAdded];
        @signal[radioRemoved];
        @signal[signalAdded];
        @signal[signalRemoved];
        @signal[signalDepartureStarted];
        @signal[signalDepartureEnded];
        @signal[signalArrivalStarted];
        @signal[signalArrivalEnded];

}
