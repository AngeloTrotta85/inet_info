//
// Copyright (C) 2013 OpenSim Ltd.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//


#include "inet/common/INETUtils.h"

#include "RadioMediumExt.h"

#include <inet/physicallayer/common/packetlevel/SignalTag_m.h>
#include <inet/physicallayer/analogmodel/packetlevel/ScalarReception.h>
#include <inet/physicallayer/base/packetlevel/ReceptionBase.h>
#include <inet/physicallayer/base/packetlevel/ScalarAnalogModelBase.h>
#include "../../../cache/GeneralCache.h"

namespace inet {

//namespace physicallayer {
using namespace physicallayer;

Define_Module(RadioMediumExt);



Packet *RadioMediumExt::receivePacket(const physicallayer::IRadio *radio, physicallayer::ISignal *signal)
{
    const ITransmission *transmission = signal->getTransmission();
    const IListening *listening = communicationCache->getCachedListening(radio, transmission);
    if (recordCommunicationLog)
        communicationLog.writeReception(radio, signal);
    const IReceptionResult *result = getReceptionResult(radio, listening, transmission);

    Packet *packet = result->getPacket()->dup();

    //EV << "I'm here!!!" << endl;
    //EV << "GOT PACKET id:" << packet->getId() << endl;
    //EV << "SNIR: min=" << getSNIR(radio, transmission)->getMin() << ", max=" << getSNIR(radio, transmission)->getMax() << endl;

    auto errorRateInd = const_cast<Packet *>(result->getPacket())->ensureTag<ErrorRateInd>();

    //EV << "BER: " << errorRateInd->getBitErrorRate() << endl;
    //EV << "PER: " << errorRateInd->getPacketErrorRate() << endl;
    //EV << "SER: " << errorRateInd->getSymbolErrorRate() << endl;

    //const IReception *rr = result->getReception();
    //EV << "RES: " << rr << endl;

    //const IInterference *interference = getInterference(radio, listening, transmission);
    //const ISnir *snir = getSNIR(radio, transmission);
    //const IReceptionDecision *receptionDecision = getReceptionDecision(radio, listening, transmission, IRadioSignal::SIGNAL_PART_WHOLE);
    //EV << "snir: MAX=" << snir->getMax() << "MIN=" << snir->getMin() << "NOISE=" << snir->getNoise() << endl;

    //EV << dynamic_cast<const ScalarAnalogModelBase *>(analogModel)->computeReceptionPower(radio, transmission, getArrival(radio, transmission));

    const ScalarReception *ss = dynamic_cast<const ScalarReception *>(result->getReception());
    //EV << "Scalar Reception. POW:" << ss->getPower() << "; BW:" << ss->getBandwidth() << "; Freq:" << ss->getCarrierFrequency() << endl;

    //cModule *cm = transmission->getTransmitter()->getRadioGate()->getOwnerModule();
    //EV << "Transmitter class: " << cm->getClassName() << endl;
    //EV << "Transmitter f name: " << cm->getFullName() << endl;
    //EV << "Transmitter f path: " << cm->getFullPath() << endl;
    //EV << "Transmitter index: " << cm->getParentModule()->getParentModule()->getIndex() << endl;

    //cModule *cm2 = radio->getRadioGate()->getOwnerModule();
    //EV << "Receiver class: " << cm2->getClassName() << endl;
    //EV << "Receiver f name: " << cm2->getFullName() << endl;
    //EV << "Receiver f path: " << cm2->getFullPath() << endl;
    //EV << "Receiver index: " << cm2->getParentModule()->getParentModule()->getIndex() << endl;


    /*ScalarReception sr = ScalarReception(radio, transmission,
            rr->getStartTime(), rr->getEndTime(),
            rr->getStartPosition(), rr->getEndPosition(),
            rr->getStartOrientation(), rr->getEndOrientation(),
            0, 0, 0);*/

    int rIdx = radio->getRadioGate()->getOwnerModule()->getParentModule()->getParentModule()->getIndex();

    Physical_Info *pi = new Physical_Info();
    pi->txAddr = transmission->getTransmitter()->getRadioGate()->getOwnerModule()->getParentModule()->getParentModule()->getIndex();
    pi->pktID = packet->getId();
    pi->pow = ss->getPower();
    pi->per = errorRateInd->getPacketErrorRate();
    pi->snir = getSNIR(radio, transmission)->getMin();
    //pi->isData = (strncmp(packet->getName(), "UDPBasicApp", 11) == 0);
    const char *appPckName = radio->getRadioGate()->getOwnerModule()->getParentModule()->getParentModule()->getSubmodule("app", 0)->par("packetName");
    pi->isData = (strncmp(packet->getName(), appPckName, strlen(appPckName)) == 0);


    //EV << "getDisplayString " << packet->getDisplayString() << endl;
    //EV << "detailedInfo " << packet->detailedInfo() << endl;
    //EV << "info " << packet->info() << endl;
    //EV << "getFullName " << packet->getFullName() << endl;
    //EV << "getName " << packet->getName() << endl;

    GeneralCache *gc = check_and_cast<GeneralCache *>(this->getParentModule()->getSubmodule("cache"));
    gc->addEntry(rIdx, GeneralCache::PHYISICAL, simTime(), pi);

    communicationCache->removeCachedReceptionResult(radio, transmission);

    delete result;
    return packet;
}

//} // namespace physicallayer

} // namespace inet

